
$(document).ready(function() {
    $("#result").attr("disabled", "disabled"); 
    ValidateInputs();  
 });


 function ValidateInputs(){
    $('#calculate_bmi').on('click', function (e) {
        e.preventDefault();
        var height =  $("input[name='height']").val();
        var weight      = $("input[name='weight']").val();
        if(height.trim() == ''){
            alert("Please enter height");
        }
        else if(weight.trim() == ''){
            alert("Please enter weight");
        }
        else if(isNaN(height) || height<0){
         alert('Please enter valid height');
        $("input[name='height']").val('');
        
         }
         else if(isNaN(weight) || weight<0){
         alert('Please enter valid weight');
        $("input[name='weight']").val('');
        
         }
         else{
         var calcBmi  = CalculateBMI(weight, height);
         if (calcBmi < 18.5) {
            document.getElementById("message").innerHTML = "You are underweight.";
         }
         else if (calcBmi > 18.5 && calcBmi < 24.9) {
             document.getElementById("message").innerHTML = "You are normal.";
         }
         else if (calcBmi > 24.9 && calcBmi < 29.99) {
             document.getElementById("message").innerHTML = "You are overweight.";
         }
         else{
             document.getElementById("message").innerHTML = "Please visit physician!";
         }
       
         $("#result").val(calcBmi);}
    });
 }

 function CalculateBMI(w, h){
  
    return (parseInt(w) /(parseInt(h)* (parseInt(h)))).toFixed(2);
  
   
 }